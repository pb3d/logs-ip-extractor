import sys
import os
import lzma
import gzip
import glob
import re

def look_for_ip(string_with_ip):
	return re.findall( r'[0-9]+(?:\.[0-9]+){3}', string_with_ip)[0]

def extract_ips(lines):
	ip_set = set()

	for line in lines:
		if ("Connection closed by" in line and "port" in line) or ("banner exchange" in line):
			ip_set.add(look_for_ip(line))

	return ip_set

def main():
	if len(sys.argv) < 2:
		print("Usage: python get_ips_from_log.py <log_filename>")
		print("       python get_ips_from_log.py <directory/>")
		exit(1)

	main_ip_set = set()
	filename_or_directory = sys.argv[1]

	if os.path.isfile(filename_or_directory):
		print(filename_or_directory)

		with open(filename_or_directory) as f:
			logs = f.readlines()

			main_ip_set = extract_ips(logs)
	elif os.path.isdir(filename_or_directory):
		print(filename_or_directory)

		for path in glob.iglob(filename_or_directory + "**/*", recursive=True):
			print(path)

			_, file_extension = os.path.splitext(path)
			extracted_ips_set = set()

			#check if directory and continue if it is
			if os.path.isdir(os.path.abspath(path)):
				continue

			#check compressions
			if file_extension == ".gz":
				#gzip
				with gzip.open(path, "rt") as f:
					logs = f.readlines()

					extracted_ips_set = extract_ips(logs)
			elif file_extension == ".xz":
				#lzma
				with lzma.open(path, "rt") as f:
					logs = f.readlines()

					extracted_ips_set = extract_ips(logs)
			else:
				#plaintext
				with open(path) as f:
					logs = f.readlines()

					extracted_ips_set = extract_ips(logs)

			main_ip_set.update(extracted_ips_set)

	with open("whitelist.txt") as whitelist:
		for ip in whitelist:
			main_ip_set.remove(ip)

	with open("ips_extracted.txt", "w") as ips_out_file:
		ips_out_file.write("\n".join(main_ip_set))

	print()
	print("Extracted into: ips_extracted.txt")

main()