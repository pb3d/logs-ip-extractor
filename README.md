# Extract "IPs" from sshd logs
Script for extracting IP addresses from plaintext, gzip or lzma compressed logs.

### Usage
```
python get_ips_from_log.py <filepath>
```
or
```
python get_ips_from_log.py <directory/>
```

The plaintext file containing extracted IP addresses is going to be named `ips_extracted.txt`.

### Logs
Logs are usually located in (dependent on linux distribution the server is using):
- `/var/log/auth*`
- `/var/log/messages*`

These are probably only readable to `root` so you can copy them to the `/tmp` directory and then `chmod 444` them so they can be downloaded with, e.g. `scp` with following commands:

1. Server

```
sudo cp /var/log/auth* /tmp/
sudo chmod 444 /tmp/auth*
```
or
```
sudo cp /var/log/messages* /tmp/
sudo chmod 444 /tmp/messages*
```

2. Client

```
scp username@host:/var/log/auth* ./logs/
OR
scp username@host:/var/log/messages* ./logs/
```

3. Server (you should probably delete widely accessible logs from everyone eyes)

```
sudo rm /tmp/auth*
OR
sudo rm /tmp/messages*
```

### Dependecies
Created and tested with: Python 3.12.3